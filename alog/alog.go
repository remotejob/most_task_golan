package alog

import "fmt"

func Info(msg string) string {
	chmessage := make(chan string, 1)
	defer close(chmessage)

	go channelData(chmessage, msg)

	return <-chmessage
}

func channelData(chmessage chan string, message string) {

	infomessage := fmt.Sprintf("info:%v", message)
	chmessage <- infomessage

}
